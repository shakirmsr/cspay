
import Pricing from "../components/pricingBlock"
import BannerWhite from "../components/bannerWhite"

import Head from "next/head"

export default function PricingDesc() {
  return (
    <>
        <Head>
            <title>CSPay | Pricing</title>
        </Head>
        <div className="content-area">
            <Pricing></Pricing>
            <BannerWhite></BannerWhite>
        </div>
    </>
  )
}
