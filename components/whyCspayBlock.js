export default function WhyCspay(){
    return (
		<div>
			<span className="anchor-spacer" id="why-cspay"></span>
			<section className="why-cspay bg-9 py-type1">
				<div className="container">
					<div className="block-header text-center">
						<h2>Why CS Pay</h2>
						<p className="p-type1">A completely integrated secure payment platform for all your payments needs!</p>
					</div>
					<div className="why-cspay__items">
						<div className="why-cspay__item">
							<div className="why-cspay__image"><img src="/images/payment-methods.svg" alt="Payment methods"/></div>
							<div className="why-cspay__desc">
								<h3>Multiple Payment Methods</h3>
								<p>Manage payments easily on a single platform while giving patients multiple payment options.</p>
								<ul className="m-0">
									<li>Securely accepts and processes all major credit and debit card payments including Visa, Mastercard, American Express, Discover, and HSA cards.</li>
									<li>Empowers your patients to pay quickly with ACH, Online, Text to Pay, or Point of Sale transactions.</li>
									<li>Accepts Cardless and Terminal payments directly integrated with your PMS.</li>
								</ul>
							</div>
						</div>
						<div className="why-cspay__item">
							<div className="why-cspay__image"><img src="/images/pms-integration.svg" alt="pms integration"/></div>
							<div className="why-cspay__desc">
								<h3>Seamless Integration with Your PMS</h3>
								<p>Simplify the processing of patient payments with our fully integrated platform.</p>
								<ul className="m-0">
									<li>CS Pay provides direct integration with your Practice Management System.</li>
									<li>Eliminates manual errors and simplifies the reconciliation process.</li>
									<li>Void and refund payments directly from the PMS without having to navigate to a separate application to refund patient payments.</li>
								</ul>
							</div>
						</div>
						<div className="why-cspay__item">
							<div className="why-cspay__image"><img src="/images/card-on-file.svg" alt="card on file"/></div>
							<div className="why-cspay__desc">
								<h3>Card on File</h3>
								<p>Now collect upfront without having to worry about write-offs or underpayments.</p>
								<ul className="m-0">
									<li>Experience hassle-free collection of future payments with &ldquo;Card tokenization&rdquo; and &ldquo;card on file&rdquo;.</li>
									<li>With the &ldquo;Auto card updater&rdquo; feature, ensure updated card information even for expired and stolen cards.</li>
									<li>Increases your cash flow and reduces your accounts receivables.</li>
								</ul>
							</div>
						</div>
						<div className="why-cspay__item">
							<div className="why-cspay__image"><img src="/images/hipaa-compliant.svg" alt="hipaa compliant"/></div>
							<div className="why-cspay__desc">
								<h3>HIPAA and PCI Compliant </h3>
								<p>Stay calm and pass over government legalese with 100% secure and confidential health data.</p>
								<ul className="m-0">
									<li>Card data is secured and tokenized from beginning to end.</li>
									<li>End-to-end encryption leverages contemporary tokenization technologies to avoid data interception by other parties.</li>
									<li>Keep patient data secure against third-party interception and fraud with HIPAA compliance &amp; patient data security protocols that are second to none.</li>
								</ul>
							</div>
						</div>
						<div className="why-cspay__item">
							<div className="why-cspay__image"><img src="/images/payment-plans.svg" alt="payment plans"/></div>
							<div className="why-cspay__desc">
								<h3>Setup payment plans</h3>
								<p>Helps you create payment plans and process payments based on the agreed payment schedules.</p>
								<ul className="m-0">
									<li>Breaks down the cost of treatment into smaller portions making payments convenient for patients.</li>
									<li>Auto Debits payments directly from your patient&rsquo;s bank account and auto posts them in your PMS ledger.</li>
									<li>Ensure regular payments for your practice and reduce aging buckets.</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="text-center">
						<a href="#" className="link link--1 link--lg" target="_blank">Book Free Demo</a>
					</div>
				</div>
			</section>
		</div>
    );
}