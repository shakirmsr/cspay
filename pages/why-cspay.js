import WhyCspay from "../components/whyCspayBlock"
import BannerBlue from "../components/bannerBlue"
import Head from "next/head"

export default function WhyCsPayDesc() {
  return (
    <>
      <Head>
          <title>CSPay | Why Cspay</title>
      </Head>
      <div className="content-area">
        <WhyCspay></WhyCspay>
        <BannerBlue></BannerBlue>
        
      </div>
    </>
  )
}