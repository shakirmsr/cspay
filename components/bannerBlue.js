export default function BannerBlue(){
    return (
        <section className="banner bg-4">
            <div className="container">
                <div className="banner__wrap">
                    <div className="banner__desc">
                        <span className="color-6">Ready To Get Started?</span>
                        <p className="color-10">Talk to a success team today and learn how CareStack can help your business.</p>
                    </div>
                    <div className="banner__link">
                        <a href="#" className="link link--2 link--md">Book Free Demo</a>
                    </div>
                </div>
            </div>
        </section>
    );
}