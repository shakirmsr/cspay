import Spotlight from "../components/spotlight"
import WhyCspay from "../components/whyCspayBlock"
import BannerBlue from "../components/bannerBlue"
import Benefits from "../components/benefitsBlock"
import Pricing from "../components/pricingBlock"
import BannerWhite from "../components/bannerWhite"

export default function Home() {
  return (
    <>
      <Spotlight></Spotlight>
      <div className="content-area">
        <WhyCspay></WhyCspay>
        <BannerBlue></BannerBlue>
        <Benefits></Benefits>
        <Pricing></Pricing>
        <BannerWhite></BannerWhite>
      </div>
    </>
  )
}
