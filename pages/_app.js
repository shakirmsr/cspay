import Layout from '../components/layout'
import '../styles/globals.css'
import '../public/sass/style.scss'
import '../loader.js'

export default function CspayApp({ Component, pageProps }) {
  return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
  )
}
