import Head from 'next/head';
import Header from "./header";
import Script from 'next/script'
import Footer from "./footer";
import {Fragment} from "react";

export default function Layout({ children }) {
    return (
        <Fragment>
            <Head>
                <meta charSet="utf-8"/>
                <meta name="description" content="An integrated patient payment solution for carestack dental practice" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <link rel="shortcut icon" href="/images/favicon/favicon.ico" />
                <title>CSPay</title>
            </Head>
            <Script src='/script/site.js'></Script>
            <div className="global-wrapper">
                <Header></Header>
                <main>{children}</main>
                <Footer></Footer>
            </div>
        </Fragment>
    )
}