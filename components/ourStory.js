export default function OurStory(){
    return(
        <section className="our-story py-type1">
					<div className="container">
						<div className="block-header text-center">
							<h1>Our Story</h1>
							<p className="p-type1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis vitae eu diam in neque maecenas.</p>
						</div>
						<div className="story">
							<div className="story__block">
								<div className="story__desc">
									<p>CareStack was founded in 2015 by Dr. Mark Huzyak and Abhi Krishna. Mark has been practicing dentistry for the past three decades, and none of the systems that existed in the market could truly keep up with the needs of his growing group practice.
									</p>
									<p>So in 2015, Mark reached out to a group of software experts led by Abhi to create a state-of-the-art, cloud-based all-in-one technology platform that fundamentally transforms the way dentists work and manage their practices.</p>
								</div>
								<div className="story__image">
									<img src="/images/members.jpg" alt="our story"/>
								</div>
							</div>
						</div>
						<div className="quote">
							<p className="quote__first">We believe that dentistry deserves tools that are as modern, well-designed, and easy-to-use.</p>
							<p className="quote__last">Our mission is to pioneer the digital transformation of the dental industry with simple, modern, cloud-based solutions.</p>
							<img src="/images/mark-d-huzyak.png" alt="mark d huzyak"/>
							<span className="quoted">Mark D Huzyak, DMD</span>
							<span className="designation">Co-Founder</span>
						</div>
					</div>
				</section>
    );
}