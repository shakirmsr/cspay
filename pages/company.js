import OurStory from "../components/ourStory"
import Numbers from "../components/numbers"
import Team from "../components/team"
import BannerBlue from "../components/bannerBlue"
import Head from "next/head"

export default function Company() {
  return (
    <>
        <Head>
          <title>CSPay | Company</title>
        </Head>
      <div className="content-area">
        <OurStory></OurStory>
        <Numbers></Numbers>
        <Team></Team>
        <BannerBlue></BannerBlue>
      </div>
    </>
  )
}