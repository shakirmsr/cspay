export default function Footer() {
    return (
        <footer className="global-footer bg-10">
			<div className="container">
				<div className="copyright">
					<span>Copyright &copy; Good Methods Global Inc. All rights reserved.</span>
				</div>
			</div>
		</footer>
    );
}