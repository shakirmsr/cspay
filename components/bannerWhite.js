export default function BannerWhite(){
    return (
        <section className="banner bg-6">
            <div className="container">
                <div className="banner__wrap">
                    <div className="banner__desc">
                        <span className="color-1">Ready To Get Started?</span>
                        <p className="color-8">Talk to a success team today and learn how CareStack can help your business.</p>
                    </div>
                    <div className="banner__link">
                        <a href="#" className="link link--1 link--md">Book Free Demo</a>
                    </div>
                </div>
            </div>
        </section>
    );
}