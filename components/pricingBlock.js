export default function Pricing(){
    return (
        <div>
            <span className="anchor-spacer" id="pricing"></span>
            <section className="pricing bg-4">
                <div className="container">
                    <div className="block-header text-center">
                        <h2 className="color-6">Pricing</h2>
                        <p className="p-type1 color-10">An all-inclusive price with no hidden costs.</p>
                    </div>
                    <div className="pricing__details">
                        <div className="pricing__item">
                            <div className="pricing__rebate">
                                <span className="pricing__variation">2.90% + $0.30</span>
                                <span className="pricing__label">For all Credit Cards (Non Amex) </span>
                            </div>
                        </div>
                        <div className="pricing__item">
                            <div className="pricing__rebate">
                                <span className="pricing__variation">3.40% + $0.30</span>
                                <span className="pricing__label">For Amex Credit Cards</span>
                            </div>
                        </div>
                        <div className="pricing__item">
                            <div className="pricing__rebate">
                                <span className="pricing__variation">0.9% + $0.15</span>
                                <span className="pricing__label">For all ACH Transactions</span>
                            </div>
                        </div>
                    </div>
                    <div className="pricing__desc">
                        <ul>
                            <li>Enjoy transparent pricing with no setup costs or hidden fees.</li>
                            <li>Increase working capital and A/R receivable with next-day funding.</li>
                            <li>What you use is what you pay for.</li>
                            <li>Real-time fee reporting.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    );
}